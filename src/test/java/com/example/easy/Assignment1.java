package com.example.easy;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Assignment1 {
    @Test
    public void validatePairsThatFormsTheKey(){

        //Arrange
        int[] arr = {1, 2, -1, -2, 4, 5, -9, 0, 10, 40};
        int key = 4;
        List<String> expectedResult = new ArrayList<>();
        expectedResult.add("{1,4}");
        expectedResult.add("{2,-2}");
        expectedResult.add("{-1,5}");
        expectedResult.add("{4,0}");
        expectedResult.add("{10,40}");

        //Act
        List<String> actualResult = new ArrayList<>();
        for(int i=0;i<arr.length;i++){
            for(int j=i+1;j<arr.length;j++){
                if(sum(arr[i],arr[j]) == key){
                    actualResult.add(getPair(arr[i],arr[j]));
                }
                else if(diff(arr[i],arr[j]) == key){
                    actualResult.add(getPair(arr[i],arr[j]));
                }
                else if(mul(arr[i],arr[j]) == key){
                    actualResult.add(getPair(arr[i],arr[j]));
                }
                else if(div(arr[i],arr[j]) == (float)key){
                    actualResult.add(getPair(arr[i],arr[j]));
                }
            }
        }

        //Assert
        int i=0;
        for (String expected : expectedResult){
            Assert.assertEquals(actualResult.get(i++),expected);
        }
    }

    public int sum(int a, int b){
        return a+b;
    }
    public int diff(int a, int b){
        return a-b;
    }
    public int mul(int a, int b){
        return a*b;
    }
    public float div(float a, float b){
        if(a>b){
            return a/b;
        }
        else{
            return b/a;
        }

    }
    public String getPair(int a, int b){
        return "{" + a + "," + b + "}";
    }
}
