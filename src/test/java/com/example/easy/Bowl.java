package com.example.easy;

import java.util.ArrayList;
import java.util.List;

public class Bowl {
    List<Fruit> fruitsBowl;
    ShapeOfBowl shapeOfBowl;

    public Bowl(){
        fruitsBowl = new ArrayList<>();
    }
    public void addFruitsToBowl(Fruit fruit){
        fruitsBowl.add(fruit);
    }

    public Bowl getBowlOfApples(){
        Bowl bowlOfApples = new Bowl();
        for(Fruit fruit : fruitsBowl){
            if(fruit instanceof Apple)
                bowlOfApples.addFruitsToBowl(fruit);
        }
        return bowlOfApples;
    }

    public Bowl getBowlOfOranges(){
        Bowl bowlOfOranges = new Bowl();
        for(Fruit fruit : fruitsBowl){
            if(fruit instanceof Orange)
                bowlOfOranges.addFruitsToBowl(fruit);
        }
        return bowlOfOranges;
    }

    public Bowl getBowlOfBananas(){
        Bowl bowlOfBananas = new Bowl();
        for(Fruit fruit : fruitsBowl){
            if(fruit instanceof Banana)
                bowlOfBananas.addFruitsToBowl(fruit);
        }
        return bowlOfBananas;
    }

    public int getCountOfFruits(){
        return fruitsBowl.size();
    }
}
