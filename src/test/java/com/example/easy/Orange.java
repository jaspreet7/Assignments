package com.example.easy;

public class Orange extends Fruit {

    private int numberOfSlices;

    @Override
    public String getColor() {
        return "Orange";
    }

    public int getNumberOfSlices() {
        return numberOfSlices;
    }

    public void setNumberOfSlices(int numberOfSlices){
        this.numberOfSlices=numberOfSlices;
    }
}
