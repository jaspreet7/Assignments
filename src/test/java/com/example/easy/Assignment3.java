package com.example.easy;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Assignment3 {

    Bowl fruitsBowl;
    private int countOfApples = 2;
    private int countOfOranges = 3;
    private int countOfBananas = 5;

    @BeforeClass
    public void createBowlOfFruits(){

        fruitsBowl = new Bowl();
        for(int i=0; i<countOfApples; i++){
            fruitsBowl.addFruitsToBowl(new Apple());
        }
        for(int i=0; i<countOfOranges; i++){
            fruitsBowl.addFruitsToBowl(new Orange());
        }
        for(int i=0; i<countOfBananas; i++){
            fruitsBowl.addFruitsToBowl(new Banana());
        }
    }

    @Test
    public void seggregateApples(){
        Bowl bowlOfApples = fruitsBowl.getBowlOfApples();
        Assert.assertEquals(bowlOfApples.getCountOfFruits(),countOfApples);
    }
    @Test
    public void seggregateOranges(){
        Bowl bowlOfOranges = fruitsBowl.getBowlOfOranges();
        Assert.assertEquals(bowlOfOranges.getCountOfFruits(),countOfOranges);
    }
    @Test
    public void seggregateBananas(){
        Bowl bowlOfBananas = fruitsBowl.getBowlOfBananas();
        Assert.assertEquals(bowlOfBananas.getCountOfFruits(),countOfBananas);
    }
}
