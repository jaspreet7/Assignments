package com.example.easy;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.*;

public class Assignment4 {
    @Test
    public void validateSeggregateByGSTAndTotalPrice(){
        
        //Arrange
        int numberOfProducts = 3;
        List<Integer> expectedProductList = new ArrayList<>();
        expectedProductList.add(3);
        List<Product> productList = new ArrayList<>();
        productList.add(new Product());
        productList.add(new Product());
        productList.add(new Product());
        
        productList.get(0).setBasePrice(Float.parseFloat("1680.45"));
        productList.get(0).setGst(Float.parseFloat("18.0"));
        
        productList.get(1).setBasePrice(Float.parseFloat("870.30"));
        productList.get(1).setGst(Float.parseFloat("0"));
      
        productList.get(2).setBasePrice(Float.parseFloat("1945.56"));
        productList.get(2).setGst(Float.parseFloat("12.0"));

        
        //Act
        List<Integer> actualProductList = new ArrayList<>();
        for(int i=0; i<numberOfProducts; i++){
            if(productList.get(i).getGst()!=0 && productList.get(i).getTotalPrice()>2000) {
                actualProductList.add(productList.indexOf(productList.get(i)) + 1);
            }
        }

        //Assert
        int i=0;
        for (Integer productNumber : expectedProductList){
            Assert.assertEquals(actualProductList.get(i++),productNumber);
        }
    }
}
