package com.example.easy;

public abstract class Fruit {
    protected String color;

    public abstract String getColor();

}
