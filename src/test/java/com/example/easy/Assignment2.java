package com.example.easy;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Assignment2 {
    @Test
    public void validateSortStudentsByAgeInGroups(){

        //Arrange
        int[] arr = {45,47,42,43,41,44,45,48};
        List<Integer> expectedGroup1= new ArrayList<>();
        expectedGroup1.add(41);
        expectedGroup1.add(42);
        expectedGroup1.add(43);
        List<Integer> expectedGroup2= new ArrayList<>();
        expectedGroup2.add(44);
        expectedGroup2.add(45);
        expectedGroup2.add(45);
        List<Integer> expectedGroup3= new ArrayList<>();
        expectedGroup3.add(47);
        expectedGroup3.add(48);
        List<List<Integer>> expectedGroupList = new ArrayList<>();
        expectedGroupList.add(expectedGroup1);
        expectedGroupList.add(expectedGroup2);
        expectedGroupList.add(expectedGroup3);


        //Act
        Arrays.sort(arr);
        int first = arr[0];
        List<List<Integer>> actualGroupList = new ArrayList<>();
        List<Integer> actualGroup= new ArrayList<>();
        actualGroup.add(first);
        for(int i=1 ; i<arr.length ; i++){
            if(arr[i] - first <=2){
                actualGroup.add(arr[i]);
            }
            else {
                actualGroupList.add(actualGroup);
                actualGroup =new ArrayList<>();
                first = arr[i];
                actualGroup.add(first);
            }
        }
        actualGroupList.add(actualGroup);


        //Assert
        int i=0;
        for (List<Integer> expectedGroup : expectedGroupList){
            int j=0;
            for (Integer expectedMember : expectedGroup){
                Assert.assertEquals(actualGroupList.get(i).get(j++),expectedMember);
            }
            i++;
        }
    }
}
