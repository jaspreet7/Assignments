package com.example.easy;

public class Product {

    private float basePrice;
    private float gst = 0;
    private float totalPrice;

    public void setBasePrice(float basePrice) {
        this.basePrice = basePrice;
    }

    public float getGst() {
        return gst;
    }

    public void setGst(float gst) {
        this.gst = gst;
    }

    public float getTotalPrice() {
        totalPrice = basePrice + ((basePrice*gst)/100);
        return totalPrice;
    }
}