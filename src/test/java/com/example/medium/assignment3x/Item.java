package com.example.medium.assignment3x;

import com.poiji.annotation.*;
import lombok.Data;

@Data
public class Item {
    @ExcelRow
    private int rowIndex;

    @ExcelCell(0)
    private String name;

    @ExcelCell(1)
    private String weekCost;

}
