package com.example.medium.assignment3x;

import com.poiji.bind.Poiji;
import com.poiji.exception.PoijiExcelType;
import org.testng.annotations.Test;

import java.io.InputStream;
import java.util.List;

public class TestExcel {

    @Test
    public void testExcel(){
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("excelData/ItemExcel.xlsx");
        List<Item> itemList = Poiji.fromExcel(inputStream, PoijiExcelType.XLSX,Item.class);

        for (Item item : itemList){
            System.out.println("RowIndex : " + item.getRowIndex());
            System.out.println("Name : " + item.getName());
            System.out.println("WeekCost : " + item.getWeekCost());
            System.out.println("\n");
        }
    }
}
