package com.example.medium.assignment2;

public class Customer {

    private String customerId;
    private Ewallet ewallet;

    public Customer(String customerId) {
        ewallet = new Ewallet();
        this.customerId = customerId;
    }

    public Ewallet getEwallet() {
        return ewallet;
    }
}
