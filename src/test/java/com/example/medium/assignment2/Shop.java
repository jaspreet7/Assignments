package com.example.medium.assignment2;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.example.utils.ReadExcelData;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Shop {

    private ShoppingCart shoppingCart;
    List<List<Object>> shopData;
    List<List<Object>> customerData;
    List<List<List<Object>>> customersData;
    private List<Item> itemList;
    private List<Customer> customerList;
    private Apple apple;
    private Milk milk;
    private Newspaper newspaper;
    private int numberOfCustomers = 1;

    @BeforeClass
    public void readDataFromExcel(){
        try {
            shopData = ReadExcelData.readDataFromExcel("ShopDataExcel").get(0);
            customersData = ReadExcelData.readDataFromExcel("CustomerCartExcel");
            customerData = customersData.get(0);
            customerData.remove(0);
            numberOfCustomers = customersData.size();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
    }

    @BeforeMethod
    public void createCustomer() {
        customerList = new ArrayList<>();
        for (int i = 0; i < numberOfCustomers; i++) {
            String customerId = LocalDateTime.now().format(DateTimeFormatter.ofPattern("YYMMDDHHmmss"));
            Customer customer = new Customer(customerId);
            customer.getEwallet().setWalletAmount(1000);
            customerList.add(customer);
        }
    }

    @BeforeMethod
    public void createItems() {

        apple = new Apple();
        milk = new Milk();
        newspaper = new Newspaper();

        apple.setQuantity((int)((double) shopData.get(1).get(1)));
        apple.setCostPerQuantity((double) shopData.get(1).get(2));
        milk.setQuantity((int)((double) shopData.get(2).get(1)));
        milk.setCostPerQuantity((double) shopData.get(2).get(2));
        newspaper.setQuantity((int)((double) shopData.get(3).get(1)));
        newspaper.setCostPerQuantity((double) shopData.get(3).get(2));

        itemList = new ArrayList<>();
        itemList.add(apple);
        itemList.add(milk);
        itemList.add(newspaper);

    }

    @Test
    public void createCustomerOrder() {
        int i = 0;
        while (i < numberOfCustomers) {
            shoppingCart = new ShoppingCart();
            int appleQuantity = (int)(double) customerData.get(0).get(1);
            int milkQuantity = (int)(double) customerData.get(1).get(1);
            int newspaperQuantity = (int)(double) customerData.get(2).get(1);

            shoppingCart.addItemsToCart(apple, appleQuantity);
            shoppingCart.addItemsToCart(milk, milkQuantity);
            shoppingCart.addItemsToCart(newspaper, newspaperQuantity);

            double totalCartCost = shoppingCart.getTotalCartAmount();
            double discountAmount = 0;
            if (totalCartCost >= 100) {
                discountAmount = totalCartCost * 0.05;
                totalCartCost = totalCartCost * 0.95;
            }

            double customerEwalletAmount = customerList.get(i).getEwallet().getWalletAmount();
            if (customerEwalletAmount > totalCartCost) {
                customerList.get(i).getEwallet().setWalletAmount(customerEwalletAmount - totalCartCost);
            } else {
                System.out.println("Wallet Amount is not sufficient. Please add amount to wallet.");
            }

            System.out.println("Order Amount : " + totalCartCost);
            System.out.println("Order Details --- ");
            shoppingCart.getCartItems().forEach(item -> System.out.println(item.getClass().getSimpleName() + " " +
                    item.orderedQuantity + " : " + Math.round(item.getTotalCost())));
            System.out.println("Total Wallet Discount : " + discountAmount);

            if (milkQuantity > 1) {
                for(Item item : shoppingCart.getCartItems()){
                    if (item instanceof Milk) {
                        System.out.println("1lt of Milk is free");
                    }
                }
            }

            Assert.assertEquals(totalCartCost,262.675);
            i++;
        }
    }

}
