package com.example.medium.assignment2;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
    private List<Item> items;
    private double totalCartAmount = 0;

    public ShoppingCart(){
        items = new ArrayList<>();
    }

    public void addItemsToCart(Item item, int quantity){
        item.setOrderedQuantity(quantity);
        items.add(item);
    }

    public double getTotalCartAmount(){
        items.forEach(item -> totalCartAmount = totalCartAmount + item.getTotalCost());
        return totalCartAmount;
    }

    public List<Item> getCartItems(){
        return items;
    }
}
