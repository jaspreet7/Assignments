package com.example.medium.assignment2;

public class Milk extends Item {

    public Milk() {
        setType();
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    protected void setType() {
        this.type = "Dairy Product";
    }

    @Override
    public void setOrderedQuantity(int orderedQuantity) {
        this.orderedQuantity = orderedQuantity;
        this.quantity = this.quantity - orderedQuantity;
    }

    @Override
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public void setCostPerQuantity(double costPerQuantity) {
        this.costPerQuantity = costPerQuantity;
    }

    @Override
    public double getTotalCost() {
        return costPerQuantity * orderedQuantity;
    }
}