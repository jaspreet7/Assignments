package com.example.medium.assignment2;

public class Newspaper extends Item {

    public Newspaper() {
        setType();
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    protected void setType() {
        this.type = "Paper";
    }

    @Override
    public void setOrderedQuantity(int orderedQuantity) {
        this.orderedQuantity = orderedQuantity;
        this.quantity = this.quantity - orderedQuantity;
    }

    @Override
    public void setQuantity(int quantity) {
        this.orderedQuantity = orderedQuantity;
        this.quantity = quantity;
    }

    @Override
    public void setCostPerQuantity(double costPerQuantity) {
        this.costPerQuantity = costPerQuantity;
    }

    @Override
    public double getTotalCost() {
        return costPerQuantity * orderedQuantity;
    }
}