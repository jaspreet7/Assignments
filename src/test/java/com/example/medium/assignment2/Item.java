package com.example.medium.assignment2;

public abstract class Item {
    protected String type;
    protected double costPerQuantity;
    protected int quantity;
    protected int orderedQuantity;
    protected double totalCost;

    public abstract String getType();

    protected abstract void setType();

    public abstract void setOrderedQuantity(int quantity);

    public abstract void setQuantity(int quantity);

    public abstract void setCostPerQuantity(double costPerQuantity);

    public abstract double getTotalCost();

}
