package com.example.medium.assignment1;

import java.util.ArrayList;
import java.util.List;

public class Opd {

    private List<Patient> patients;

    public Opd(){
        patients = new ArrayList<>();
    }

    public List<Patient> getPatients() {
        return patients;
    }

    public void addPatient(Patient patient){
        patients.add(patient);
    }
}
