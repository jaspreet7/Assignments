package com.example.medium.assignment1;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.example.utils.ReadExcelData;

import java.io.IOException;
import java.util.List;

public class Hospital {

    Patient patient;
    Opd opd = new Opd();

    @BeforeClass
    public void createPatientData() {
        try {
            List<List<List<Object>>> dataTable = ReadExcelData.readDataFromExcel("HospitalDataExcel");
            dataTable.get(0).remove(0);
            for (List<Object> rowData : dataTable.get(0)) {
                patient = new Patient();
                patient.setPatientId((String) rowData.get(0));
                patient.setPatientLocation((String) rowData.get(1));
                opd.addPatient(patient);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void calculateLocalPatientPercentage() {
        List<Patient> patientList = opd.getPatients();
        int totalNumberOfPatients = patientList.size();
        int countOfBangalorePatients = 0;
        for (Patient patient : patientList) {
            if (patient.getPatientLocation().equals("Bangalore")) {
                countOfBangalorePatients++;
            }
        }

        double localPatientPercentage = ((double) countOfBangalorePatients / (double) totalNumberOfPatients) * 100;
        System.out.println(totalNumberOfPatients + " OP registrations took place of which " + localPatientPercentage +
                "% are from bangalore");

        Assert.assertEquals(localPatientPercentage,70.0);
    }
}
