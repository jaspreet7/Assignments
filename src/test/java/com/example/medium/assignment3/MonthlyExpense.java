package com.example.medium.assignment3;

import com.example.utils.ReadExcelData;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.io.IOException;
import java.util.*;

public class MonthlyExpense {
    private Map<String,List<List<Object>>> itemsData;
    private List<List<List<Object>>> consumersData;
    private List<List<Object>> newsPaperData;
    private Map<Integer, Consumer> consumerMap;
    private Map<String,Item> itemsMap;
    private Map<String, Double> newsPaperExpense = new HashMap<>();

    @BeforeClass
    public void setupDataForTest(){
        readDataFromExcel();
        setItemsData();
        calculateMonthlyExpenseOfEachNewsPaper();
        setConsumersData();
    }

    @Test
    public void getMonthlyNewspaperExpenseOfConsumer() {
        for (Map.Entry<Integer, Consumer> consumer : consumerMap.entrySet()){
            double monthlyExpense = 0;
            for (String newspaper : consumer.getValue().getItemListMap().get("Newspaper")){
                monthlyExpense = monthlyExpense + newsPaperExpense.get(newspaper);
            }
            System.out.println("Monthly Expense Of Consumer " + consumer.getKey() + " : " + monthlyExpense);
        }
    }

    private void readDataFromExcel() {
        try {
            itemsData = ReadExcelData.readDataWithSheetNameFromExcel("ItemWeeklyDataExcel");
            consumersData = ReadExcelData.readDataFromExcel("ConsumerDataExcel");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
    }

    private void setItemsData(){
        itemsMap = new HashMap<>();
        for (Map.Entry<String,List<List<Object>>> entry : itemsData.entrySet()){
            Item item = new Item();
            item.setType(entry.getKey());
            Item childItem = item.getType();
            entry.getValue().remove(0);
            Map<String,List<Object>> childItemMap = new LinkedHashMap<>();
            for (List<Object> eachChildItem : entry.getValue()){
                String name = (String) eachChildItem.get(0);
                eachChildItem.remove(0);
                childItemMap.put(name,eachChildItem);
            }
            childItem.setItemsWeekCostMap(childItemMap);
            itemsMap.put(entry.getKey(), childItem);
        }
    }

    private void calculateMonthlyExpenseOfEachNewsPaper() {
        Weekdays weekdays = new Weekdays();
        Map<Integer, Integer> numberOfWeekdaysInMonth = weekdays.getNumberOfWeekdaysInMonth();
        Map<String,List<Double>> newsPaperData = itemsMap.get("Newspaper").getItemsWeekCostMap();
        for (Map.Entry<String,List<Double>> newspaper: newsPaperData.entrySet()) {
            double expense = 0;
            for (int i = 0; i < 7; i++) {
                expense = expense + newspaper.getValue().get(i) * numberOfWeekdaysInMonth.get(i);
            }
            newsPaperExpense.put(newspaper.getKey(),expense);
        }
    }

    private void setConsumersData() {
        consumerMap = new LinkedHashMap<>();
        for (List<List<Object>> consumerData : consumersData) {
            consumerData.remove(0);
            Map<String, List<String>> itemMap = new HashMap<>();
            for (List<Object> data : consumerData) {
                List<String> itemList = Arrays.asList(((String) data.get(1)).replaceAll(" ", "").
                        split(","));
                itemMap.put((String) data.get(0), itemList);
            }
            Consumer consumer = new Consumer();
            consumer.setConsumerNumber(consumer.hashCode());
            consumer.setItemListMap(itemMap);
            consumerMap.put(consumer.getConsumerNumber(), consumer);
        }
    }
}
