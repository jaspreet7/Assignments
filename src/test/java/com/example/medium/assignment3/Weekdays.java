package com.example.medium.assignment3;

import java.util.*;

public class Weekdays {

    private Map<Integer,Integer> numberOfWeekdaysInMonth;

    public Weekdays(){
        numberOfWeekdaysInMonth = new TreeMap<>();
    }

    public Map<Integer,Integer> getNumberOfWeekdaysInMonth(){
        Calendar calendar = Calendar.getInstance();
        int totalDaysInMonth = calendar.getMaximum(Calendar.DAY_OF_MONTH);
        int weekOfMonth = 0;
        for (int i=1; i<=totalDaysInMonth;i++){
            calendar.set(Calendar.DAY_OF_MONTH,i);
            int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
            if (numberOfWeekdaysInMonth.get(dayOfWeek) != null){
                numberOfWeekdaysInMonth.put(dayOfWeek,numberOfWeekdaysInMonth.get(dayOfWeek) + 1);
            } else {
                numberOfWeekdaysInMonth.put(dayOfWeek,1);
            }
        }
       return numberOfWeekdaysInMonth;
    }
}

