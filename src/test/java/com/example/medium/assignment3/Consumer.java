package com.example.medium.assignment3;

import org.reflections.Reflections;
import java.util.*;

public class Consumer {

    private int consumerNumber;
    private Map<String, List<String>> itemListMap;

    public Consumer() {
        itemListMap = new HashMap<>();
    }

    public Map<String, List<String>> getItemListMap() {
        return itemListMap;
    }

    public void setItemListMap(Map<String, List<String>> itemListMap) {
        this.itemListMap = itemListMap;
    }

    public int getConsumerNumber() {
        return consumerNumber;
    }

    public void setConsumerNumber(int consumerNumber) {
        this.consumerNumber = consumerNumber;
    }

}
