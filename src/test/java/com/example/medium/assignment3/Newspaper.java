package com.example.medium.assignment3;

public class Newspaper extends Item<Newspaper> {

    private String newspaperName;
    private double monthlyCost;

    public Newspaper(){
    }
    public String getNewspaperName() {
        return newspaperName;
    }

    public void setNewspaperName(String newspaperName) {
        this.newspaperName = newspaperName;
    }

    public double getMonthlyCost() {
        return monthlyCost;
    }

    public void setMonthlyCost(double monthlyCost) {
        this.monthlyCost = monthlyCost;
    }
}
