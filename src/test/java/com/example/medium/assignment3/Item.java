package com.example.medium.assignment3;

import org.reflections.Reflections;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Item<T extends Item> {
    private T type;
    private Map<String, List<Double>> itemsWeekCostMap;

    public Map<String, List<Double>> getItemsWeekCostMap() {
        return itemsWeekCostMap;
    }

    public void setItemsWeekCostMap(Map<String, List<Double>> itemsWeekCostMap) {
        this.itemsWeekCostMap = itemsWeekCostMap;
    }

    public T getType() {
        return this.type;
    }

    public void setType(String type) {
        Reflections reflections = new Reflections(Item.class.getPackageName());
        Iterator<Class<? extends Item>> iterator = reflections.getSubTypesOf(Item.class).iterator();
        while (iterator.hasNext()) {
            Class<? extends Item> cl = iterator.next();
            Object obj = null;
            if (cl.getSimpleName().equals(type)) {
                try {
                    Constructor<?> constructor = cl.getDeclaredConstructor();
                    constructor.setAccessible(true);
                    obj = constructor.newInstance();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            this.type = ((T) obj);
        }
    }
}
